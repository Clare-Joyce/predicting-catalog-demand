# Predicting Catalog Demand

Faced with the task of making a decision whether to send out catalogs to the newly acquired 250 customers or not, a company tries to predict the profit that will be generated from these customers.

## Tools used
- Python
- Jupyter Notebook
- Alteryx


## **Step 1: Business and Data Understanding**

### **Key Decisions:**

**1. What decisions needs to be made?**

- Should the company send catalogs to the 250 new customers or not.
- This decision depends on whether the expected profit from these 250 new customers exceeds $10,000 or not.

**2. What data is needed to inform those decisions?**

From the problem presented, and what the analyst is expected to predict, the following information is determined to be among the features needed to do the prediction.

- Information about current customers is required (average number of products bought by previous customers, the average sales made from each customer, etc.).
- The number of years a customer has been buying from this company can also have an impact on the average sales made from the customer.
- At the end of the analysis, the profit is required, hence info about the cost is needed and sales is needed.
- The decision of sending a catalog or not has to be made. These catalogs are produced at a cost. What is the cost of printing and distributing a catalog? It makes sense for one to conclude that the cost of distribution will not be the same for all customers hence the average cost of printing and distributing will help a great deal.

## **Step 2: Analysis, Modeling, and Validation**

**1. Feature Selection:** 
[OLS regression](https://www.statsmodels.org/dev/generated/statsmodels.regression.linear_model.OLS.html) summaries coupled with several other metrics(listed below) were very helpful in deciding the feature importance.

- Scatterplots
- Correlation
- p-values
- r-squared values

![](./pictures/Picture1.png)

_Figure 1: Structure of the provided dataset_

**Name** : Intuitively, this should not have any impact on the average sales from each customer with respect to the available dataset.

**Customer\_ID** : This is a unique identification number allocated to each customer which does not have any effect on the sales made.

**Address** : The dataset of 2375 entries have 2321 different addresses; it is almost like a unique identification number in this case and hence dropped.

**State** : It is a common feature among the customers.

**City** : An OLS regression using this variable (using dummy variables) showed that all dummy variables associated with this variable are statistically insignificant considering a significance level of 0.05 and hence, dropped.

**ZIP** : Converted it to a categorical variable and proceeded with OLS regression. None of the dummy variable were statistically significant and the r-squared ( was also poor. Hence, dropped.

**Store\_Number** : Results from the scatterplot and the OLS on categorical Store\_Number indicated it was not a good predictor.

![](./pictures/Picture2.png) ![](./pictures/Picture3.png)

_Figure 2: Scatter plot / regression results-Store\_Number_

**Responded\_to\_Last\_Catalog:** All new customers are yet to access the catalog but out of curiosity` an OLS regression was performed.

![](./pictures/Picture4.png) ![](./pictures/Picture5.png)

_Figure 3: Regression results / Average sales made - Responded\_to\_Last\_Catalog_

This variable is statistically significant but a very low r-squared communicates a not so good linear relationship between this variable and the target variable.

**#\_Years\_as\_Customer** : From the correlation, OLS regression results and the scatter plot, #\_years\_as\_customer is not statistically significant.

![](./pictures/Picture6.png) ![](./pictures/Picture7.png)

_Figure 4:Scatter plot / regression results - #\_Years\_as Customer_

A p-value of 0.147 shows this variable is statistically insignificant when predicting the average sales amount. The very poor r-squared (0.001) indicates a weak explanatory power of the model

**2. Justifying the Model**

The selection process was based on the p-values **AND** the r-squared values. This ended with two statistically significant variables and two models with a good explanatory power. The r-squared value from the overall model indicates that these two variables put together have a stronger explanatory capability of the variable.

**Avg\_Num\_Products\_Purchased** : The p-value from OLS analysis shows this variable is statistically significant and the r-squared (0.732) suggests a strong linear relationship between these two variables

![](./pictures/Picture8.png) ![](./pictures/Picture9.png)

_Figure 5:Scatter plot / regression results -Avg\_Num\_Products\_Purchased_

**Customer\_Segments** : Performing an OLS regression after generating the dummy variables produces the following results.

![](./pictures/Picture10.png)

_Figure 6: Customer segments OLS results._

The p-values indicate statistical significance of the dummy variables. This variable can explain 70.2% variation in the average sale amount, hence, a good predictor.

| **Selected Variable** | **p-value** | **r-squared** |
| --- | --- | --- |
| **Avg\_Num\_Products\_Purchased** | 0.000 | 0.732 |
| **Customer\_Segments** | 0.000 | 0.702 |

_Figure 7: Selected variables, p-values, r-squared values._

![](./pictures/Picture11.png)

_Figure 8: Final model regression stats._

The adjusted r-squared (a metric, which does not increase with number of variables) of this model also indicates the model is a better fit. A zero **prob(F-Statistic)** which basically tests the overall significance of the model instigates a better model.

**3.**** Best linear regression equation**

![](./pictures/Picture12.png)

_Figure 9: Equation of the linear model._

## **Step 3: Presentation/Visualization**

![](./pictures/Picture13.png) ![](./pictures/Picture14.png)

_Figure 10: Distribution of # of Years as Customer/Distribution of Average Number of Products Purchased_

The number of years as customer exhibits an almost uniform distribution implying, we have almost the same number of customers for each #\_years\_as\_customer value. Moving on to the average number of products purchased, the distribution is right skewed. This histogram tells us that as the number of people buying drops as the number of products purchased increases. This totally makes sense as the number of products do not only increase, but the price or amount of money also does.

![](./pictures/Picture15.png) ![](./pictures/Picture16.png)

_Figure 11: Customer segment distribution and sales from each segment._

The figures above represent the distribution of customer segment and the sum of the average sale amount, respectively. It is evidently that the **store mailing list** segment has the greatest number of customers and yet produces the least average sale amount. This is the opposite when looking at the **loyalty club and credit card** segment.

![](./pictures/Picture17.png)

_Figure 12: Distribution of the responded to last catalog variable_

This shows the distribution of **responded to last catalog** variable. It clearly shows that most customers did not respond to the catalog. When the relationship between this variable and the target variable was modelled, the p-value showed that this was a statistically significant variable and a close to zero prob (F-Statistic) was proof of the model being meaningful. However, the very low r-squared showed that this variable could explain very little variation in the average sale amount. This to some extent presents an ambiguity. It will be interesting to know what these statistics will look like if we have comparable number of customers in the two groups.

**1. Recommendation**

I will recommend that we proceed with the catalog distribution.

**2. How did you come up with your recommendation?**

- Used data collected from previous customers to build a Linear Regression model
- This model was used to predict the average sale amount from the incoming customers.
- Looking at the probability that a customer will purchase from us, I calculated how much revenue we expect from each customer.

- It was mentioned that the average gross margin (price - cost) on all products sold through the catalog is 50%. I went further to calculate the revenue expected from each customer taking into consideration the gross margin.

- The profit assuming the catalogs are sent to the 250 customers and the cost of printing and distributing each is $6.50. The expected profit from each customer will be

**3. Expected profit**

From my analysis and calculations, the expected profit will be around $21987.44. This exceeds the $10,000 initially placed by the manager as their determining factor for deciding to send out catalogs or not. Hence, my recommendation.
